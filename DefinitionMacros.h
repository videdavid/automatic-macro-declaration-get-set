#ifndef DEFINITION_MACROS_H
#define DEFINITION_MACROS_H

#define PRIVATE(type, accessor, name, faceName)\
    private:\
        type name = {};\
    public:\
		accessor(name,type, faceName)\

#define PROTECTED(type, accessor, name, faceName)\
    protected:\
        type name = {};\
    public:\
		accessor(name,type, faceName)\

#define GET(name, type, faceName)\
	const type Get##faceName() const{ \
            return name;\
	};\

#define SET(name,type, faceName)\
	void Set##faceName(type value){ \
	name = value;\
	};\

#define GET_SET(name, type, faceName)\
	const type Get##faceName() const{ \
		return name;\
	};\
	void Set##faceName(type value){ \
		name = value;\
	};\

#define NONE(name,type,faceName)
	
#endif
